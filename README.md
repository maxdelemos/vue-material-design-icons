# Vue Material Design Icon Size Components

This library is just simple fork: https://gitlab.com/robcresswell/vue-material-design-icons
which was added the possibility of manipulating the [size](#props) of the icons.



## Getting started

1. Install the package

    ```console
    npm install --save vue-material-design-icons-size
    ```

2. **Optional, but recommended** Add the included stylesheet to your root JS
   file, usually `index.js` or `main.js`:

    ```javascript
    import "vue-material-design-icons-size/styles.css"
    ```

3. Import the icon, and declare it as a local component:

    ```javascript
    import MenuIcon from "vue-material-design-icons-size/Menu.vue"

    components: {
      MenuIcon
    }
    ```

    **OR**

    Declare it as a global component:

    ```javascript
    import MenuIcon from "vue-material-design-icons-size/Menu.vue"

    Vue.component("menu-icon", MenuIcon)
    ```

    > **Note** Icon files are pascal cased, e.g. `CheckboxMarkedCircle.vue`, and
    > their default name has `Icon` appended e.g. `CheckboxMarkedCircleIcon`.

4. Then use it in your template code!

    ```html
    <menu-icon />
    ```

## Props

* `width` - This property lets you set the height of the icon.

    Example:

    ```html
    <android-icon width="'30'"
    ```
* `height` - This property lets you set the height of the icon.

    Example:

    ```html
    <android-icon width="'30'"
    ```

* `title` - This changes the hover tooltip as well as the title shown to screen
  readers. By default, those values are a "human readable" conversion of the
  icon names; for example `chevron-down-icon` becomes "Chevron down icon".

    Example:

    ```html
    <android-icon title="this is an icon!" />
    ```
   
* `decorative` - This denotes whether an icon is purely decorative, or has some
  meaninfgul value. If an icon is decorative, it will be hidden from screen
  readers. By default, this is `false`.

    Example:

    ```html
    <android-icon decorative />
    ```

* `fillColor` - This property allows you to set the fill colour of an icon via
  JS instead of requiring CSS changes. Note that any CSS values, such as
  `fill: currentColor;` provided by the optional CSS file, may override colours
  set with this prop.

    Example:

    ```html
    <android-icon fillColor="#FF0000"
    ```

## Icons

A list of the icons can be found at the
[Material Design Icons website](https://materialdesignicons.com/
"Material Design Icons website"). The icons packaged here are pascal cased
versions of the names displayed on the website, to match the
[Vue Style Guide](https://vuejs.org/v2/style-guide/). For example, the icon
named `ultra-high-definition` would be imported as
`"vue-material-design-icons-size/UltraHighDefinition.vue"`.

## Tips

- Use `resolve` in your Webpack config to clean up the imports:

    ```javascript
    resolve: {
      alias : {
        "icons": path.resolve(__dirname, "node_modules/vue-material-design-icons-size")
      },
      extensions: [
        ".vue"
      ]
    }
    ```

    This will give you much shorter and more readable imports, like
    `import Android from "icons/Android"`, rather than
    `import Android from "vue-material-design-icons-size/Android.vue"`. Much better!

- Add click handlers to the icons with `@click.native`. For example:

    ```html
      <fullscreen-icon @click.native="myMethod" />
    ```

    You can learn more about this by reading the Vue docs on 
    [Binding Native Events to Components]
    (https://vuejs.org/v2/guide/components.html#Binding-Native-Events-to-Components)

